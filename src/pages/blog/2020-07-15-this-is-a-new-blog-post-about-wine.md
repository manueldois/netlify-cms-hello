---
templateKey: blog-post
title: This is a new blog post about Wine
date: 2020-07-15T15:25:13.043Z
description: Wine is a program to run windows apps inside linux
featuredpost: true
featuredimage: /img/lolipop.jpg
tags:
  - Wine
---
Wine (originally an acronym for "Wine Is Not an Emulator") is a compatibility layer capable of running Windows applications on several POSIX-compliant operating systems, such as Linux, macOS, & BSD. Instead of simulating internal Windows logic like a virtual machine or emulator, Wine translates Windows API calls into POSIX calls on-the-fly, eliminating the performance and memory penalties of other methods and allowing you to cleanly integrate Windows applications into your desktop.